// mdnum is a command line tool to add section numbers to the headings
// in markdown (.md) files.

package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
)

const (
	POUND = byte(35)
	ZERO  = byte(48)
	NINE  = byte(57)
	SPACE = byte(32)
	DOT   = byte(46)
	TILDE = byte('~')
	BACKT = byte('`')
)

var NL = []byte("\n")

func main() {
	// init counter
	cm := make(map[int]int)
	depthCur := 0
	depthNxt := 0

	scanner := bufio.NewScanner(os.Stdin)
	lineNumber := 0
	for scanner.Scan() {
		lineNumber++
		b := scanner.Bytes()

		// ignore inside code blocks
		if cbStart := lnIsCodeBlock(b); cbStart != 0 {
			lnToStdout(b)
			ignoreCodeBlock(scanner, cbStart)
			continue
		}

		// ck if heading
		if len(b) == 0 || b[0] != POUND {
			lnToStdout(b)
			continue
		}

		// get heading depth & check for broken semantics
		depthNxt = getHeadingDepth(b)
		if depthNxt-depthCur > 1 {
			fmt.Fprintf(
				os.Stderr,
				"semantic broken line %d: depth %d after %d\n",
				lineNumber, depthNxt, depthCur,
			)
			lnToStdout(b)
			continue
		}

		// update counter-map
		if depthNxt < depthCur {
			for i := depthNxt + 1; i <= depthCur; i++ {
				delete(cm, i)
			}
		}
		depthCur = depthNxt
		cm[depthCur]++

		// write new heading ln to stdout
		hToStdout(depthCur, cm, bytes.TrimLeft(b, ".# 0123456789"))
	}
}

// write new heading to stdout
func hToStdout(d int, cm map[int]int, b []byte) {

	buf := bytes.NewBuffer(make([]byte, 0, 64))
	buf.Write(bytes.Repeat([]byte("#"), d))
	buf.WriteByte(SPACE)
	for i := 1; i < 24; i++ {
		v, ok := cm[i]
		if !ok {
			break
		}
		buf.WriteString(strconv.Itoa(v))
		buf.WriteByte(DOT)
	}
	buf.WriteByte(SPACE)
	buf.Write(b)
	os.Stdout.Write(buf.Bytes())
	os.Stdout.Write(NL)
}

// write ln + newline to stdout
func lnToStdout(ln []byte) {
	os.Stdout.Write(ln)
	os.Stdout.Write(NL)
}

// return the number of '#' prefixing a line
func getHeadingDepth(ln []byte) int {
	for i := 0; i < len(ln); i++ {
		if ln[i] != POUND {
			return i
		}
	}
	return len(ln)
}

// test if line is beginning/end of code block
// returns byte of code block or 0
func lnIsCodeBlock(ln []byte) byte {
	if len(ln) < 3 {
		return 0
	}

	for i, ch := range ln[:3] {
		if ch != TILDE {
			break
		}
		if i == 2 {
			return TILDE
		}
	}
	for i, ch := range ln[:3] {
		if ch != BACKT {
			break
		}
		if i == 2 {
			return BACKT
		}
	}

	return 0
}

// read lines from scanner until code block signified by
// 3*cb ends
func ignoreCodeBlock(scanner *bufio.Scanner, cbStart byte) {
	for scanner.Scan() {
		b := scanner.Bytes()
		cbEnd := lnIsCodeBlock(b)
		if cbEnd == cbStart {
			lnToStdout(b)
			return
		}
		lnToStdout(b)
	}
}
