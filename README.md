# About

Add section numbering to markdown files. File read from stdin, numbered version written to stdout.

Lines breaking header semantics (eg. `###` after `#` with no preceeding `##`) are printed unchanged, and reported to stderr.

Titles in code-blocks are ignored.

Already numbered titles are stripped of numbering before processing, allowing changing of documents later.


# Example

```sh
$ mdnum <<DOC
> # main title
> ...
> ## subtitle
> ...
> ## another subtitle
> ...
> ### sub sub title
> ...
> ## third subtitle
> ...
> # main title two
> DOC
```

Output:

```sh
# 1. main title
...
## 1.1. subtitle
...
## 1.2. another subtitle
...
### 1.2.1. sub sub title
...
## 1.3. third subtitle
...
# 2. main title two
```
